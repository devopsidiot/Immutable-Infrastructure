#Sets region
# provider "aws" {
#   profile = "default"
#   region  = "us-east-1"
# }

#Create VPC
resource "aws_vpc" "version2" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "VPC-v2"
  }
}

#Creates subnets within previously created VPC
resource "aws_subnet" "version2-subnet-a" {
  vpc_id                  = "${aws_vpc.version2.id}"
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "VPC-Subnet1"
  }
}

# resource "aws_subnet" "version2-subnet-b" {
#   vpc_id            = "${aws_vpc.version2.id}"
#   cidr_block        = "10.0.2.0/24"
#   availability_zone = "us-east-1b"
#
#   tags = {
#     Name = "VPC-Subnet2"
#   }
# }
#
# resource "aws_subnet" "version2-subnet-c" {
#   vpc_id            = "${aws_vpc.version2.id}"
#   cidr_block        = "10.0.3.0/24"
#   availability_zone = "us-east-1c"
#
#   tags = {
#     Name = "VPC-Subnet3"
#   }
# }

resource "aws_internet_gateway" "version2-igw" {
  vpc_id = "${aws_vpc.version2.id}"

  tags = {
    Name = "Version2 IGW"
  }
}

#Definition of route table
resource "aws_route_table" "version2-rt" {
  vpc_id = "${aws_vpc.version2.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.version2-igw.id}"
  }
  tags = {
    Name = "Version 2 Route Table - Public"
  }
}

#Assigning route table to subnet-a [considered as public subnet]
resource "aws_route_table_association" "version2-publicrt" {
  subnet_id      = "${aws_subnet.version2-subnet-a.id}"
  route_table_id = "${aws_route_table.version2-rt.id}"
}

#Creation of application load balancer referencing 3 subnets
# resource "aws_lb" "version2-alb" {
#   name               = "Version2-alb"
#   internal           = false
#   load_balancer_type = "application"
#   subnets            = ["${aws_subnet.version2-subnet-a.id}", "${aws_subnet.version2-subnet-b.id}", "${aws_subnet.version2-subnet-c.id}"]
# }
