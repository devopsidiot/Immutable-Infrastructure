# Immutable-Infrastructure

Concept of automating the following: 
1. Build of an AWS AMI w/ Packer
2. Build of AWS VPC w/ Terraform
3. Build of AWS EC2 w/ Terraform that references previous AMI build w/ Packer