provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

data "aws_ami" "Testv1" {
  most_recent = true
  owners      = ["self"]
  filter {
    name   = "name"
    values = ["Ubuntu 16.04 with Docker/Swarm/awscli/nfs-common(v1)"]
  }
}

output "ami_id" {
  value = "${data.aws_ami.Testv1.id}"
}

resource "aws_instance" "foo" {
  ami           = "${data.aws_ami.Testv1.id}"
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.version2-subnet-a.id}"
}
